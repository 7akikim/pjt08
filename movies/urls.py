from django.urls import path
from . import views

app_name = 'movies'
urlpatterns = [
    path('', views.index, name='index'),
    path('new/', views.create, name='create'),
    path('<int:movie_pk>/', views.detail, name='detail'),
    path('<int:movie_pk>/edit', views.update, name='update'),
    path('<int:movie_pk>/delete', views.delete, name='delete'),
    path('<int:movie_pk>/scores/new', views.scores_new, name='scores_new'),
    # path('<int:movie_pk>/scores/<int:rates_pk>/delete', views.scores_delete, name='scores_delete'),  
]
