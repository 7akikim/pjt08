from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Avg
from .models import Genre, Movie, Score
from .forms import MovieForm, ScoreForm

# Create your views here.
def index(request):
    movies = Movie.objects.annotate(score_avg=Avg('score__score')).all()
    context = {'movies': movies}
    return render(request, 'movies/index.html', context)

def create(request):
    if request.method == 'POST':
        form = MovieForm(request.POST)
        if form.is_valid():
            movie = form.save()
            return redirect('movies:detail', movie.pk)
    else:
        form = MovieForm()
    context = {'form': form}
    return render(request, 'movies/form.html', context)

def detail(request, movie_pk):
    # 1. annotate 활용하여 추가 칼럼을 score_avg를 score의 score 평균으로 설정. 
    # template에서는 movie.score_avg로 활용할 것.
    # 쿼리 2 
    movie = get_object_or_404(Movie.objects.annotate(score_avg=Avg('score__score')), pk=movie_pk)
    scores = movie.score_set.all()
    form = ScoreForm()
    context = {'movie': movie, 'scores': scores, 'form': form}

    # 2. 설명하기는 좋을지 모르지만..
    # template에서는 score_avg로 활/용할 것.
    # 쿼리 3
    # movie = Movie.objects.get(pk=movie_pk)
    # scores = movie.score_set.all()
    # score_avg = movie.score_set.aggregate(Avg('score')).get('score__avg')
    # context = {'movie': movie, 'scores': scores, 'score_avg': score_avg}

    return render(request, 'movies/detail.html', context)
    

def update(request, movie_pk):
    movie = get_object_or_404(Movie, pk=movie_pk)
    if request.method == 'POST':
        form = MovieForm(request.POST, instance=movie)
        if form.is_valid():
            movie = form.save()
            return redirect('movies:detail', movie.pk)
    else:
        form = MovieForm(instance=movie)
    context = {'form': form}
    return render(request, 'movies/form.html', context)

def delete(request, movie_pk):
    movie = get_object_or_404(Movie, pk=movie_pk)
    movie.delete()
    return redirect('movies:detail', movie_pk)

def scores_new(request, movie_pk):
    movie = get_object_or_404(Movie, pk=movie_pk)
    if request.method == 'POST':
        '''
        방법 1. 
        form = ScoreForm(request.POST)
        if form.is_valid():
            score = form.save(commit=False)
            score.movie = movie
            score.save()
        '''
        # 방법 2.
        score = Score(movie=movie)
        form = ScoreForm(request.POST, instance=score)
        if form.is_valid():
            score = form.save()
            return redirect('movies:detail', movie.pk)

    return redirect('movies:detail', movie_pk) 
    ''' 
    # 굳이 오류 메세지 보여주고싶다면, 중복 코드밖에 생각 안나네여.
    # movie = Movie.objects.annotate(score_avg=Avg('score__score')).get(pk=movie_pk)
    # scores = movie.score_set.all()
    # context = {'movie': movie, 'scores': scores, 'form': form}
    # return render(request, 'movies/detail.html', context)
    '''